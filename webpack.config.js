const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: path.join(__dirname, 'src', 'public', 'main.js'),
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	mode: 'development',
	resolve: {
		extensions: ['.jsx', '.js']
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{loader: "style-loader"}, // creates style nodes from JS strings
					{loader: "css-loader"}, // translates CSS into CommonJS
					{loader: "sass-loader"} // compiles Sass to CSS
				]
			},
			{
				test: /\.jsx?/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {presets: ['babel-preset-react', 'babel-preset-es2015']}
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.join(__dirname, 'src', 'views', 'index.html')
		})
	]
};